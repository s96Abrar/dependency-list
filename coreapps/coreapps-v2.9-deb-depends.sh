#/bin/bash

# Basic Packages which needed for all coreapps
sudo apt install git g++ qt5-qmake qtbase5-dev qtbase5-dev-tools

# LibArchive-Qt
sudo apt install libarchive-dev liblzma-dev libbz2-dev zlib1g-dev liblz-dev lzop

# LibCPrime
sudo apt install qtmultimedia5-dev qtconnectivity5-dev libnotify-dev

## LibCSys
sudo apt install udisks2

# CoreAction
sudo apt install libqt5svg5-dev

# CoreInfo
sudo apt install libmediainfo-dev libzen-dev zlib1g-dev

# CoreKeyBoard
sudo apt install libqt5x11extras5-dev libxtst-dev libx11-dev

# CorePdf
sudo apt install libpoppler-qt5-dev qtbase5-private-dev

# CoreShot
sudo apt install libqt5x11extras5-dev

# CoreStats
sudo apt install libsensors4-dev

# CoreStuff
sudo apt install libxcb-util0-dev libxcb-ewmh-dev libxcb-icccm4-dev libx11-dev libxi-dev libxcomposite-dev libkf5globalaccel-dev

# CoreTerminal
sudo apt install libqtermwidget5-0-dev

# CoreTime
sudo apt install qtmultimedia5-dev

# CoreToppings
sudo apt install qtmultimedia5-dev libqt5x11extras5-dev qtconnectivity5-dev libpulse-dev libxdamage-dev libxrender-dev libxcb-damage0-dev

