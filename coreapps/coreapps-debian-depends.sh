#!/bin/bash

# Debian
# Shell script for --no-install-recommends installing dependency list for CuboCore/CoreApps (https://gitlab.com/cubocore)

apt-get update -yq

# General
apt-get --no-install-recommends install -y g++ git ca-certificates make cmake ninja-build extra-cmake-modules

# Qt base
apt-get --no-install-recommends install -y qt5-qmake qtbase5-dev qtbase5-dev-tools qtbase5-private-dev qtchooser

# CoreAction
apt-get --no-install-recommends install -y libqt5svg5-dev

# CoreInfo
apt-get --no-install-recommends install -y libmediainfo-dev libzen-dev zlib1g-dev

# CoreKeyBoard
apt-get --no-install-recommends install -y libqt5x11extras5-dev libxtst-dev libx11-dev

# CorePDF
apt-get --no-install-recommends install -y libpoppler-qt5-dev

# CoreShot
# apt-get --no-install-recommends install -y libqt5x11extras5-dev

# CoreStats
apt-get --no-install-recommends install -y libsensors4-dev

# CoreStuff
apt-get --no-install-recommends install -y libxcb-util0-dev libxcb-ewmh-dev libxcb-icccm4-dev libx11-dev libxi-dev libxcomposite-dev libkf5globalaccel-dev

# CoreTerminal
apt-get --no-install-recommends install -y libqtermwidget5-0-dev libqt5serialport5-dev libutf8proc-dev

# CoreTime
# apt-get --no-install-recommends install -y qtmultimedia5-dev

# CoreToppings
apt-get --no-install-recommends install -y qtmultimedia5-dev qtlocation5-dev libqt5x11extras5-dev qtconnectivity5-dev libpulse-dev libxrender-dev libxcb-util0-dev libxcb-damage0-dev libxdamage-dev libdbusmenu-qt5-dev

# LibArchive-Qt
apt-get --no-install-recommends install -y libarchive-dev liblzma-dev libbz2-dev zlib1g-dev liblz-dev lzop

# LibCPrime
# apt-get --no-install-recommends install -y

# LibCSys
apt-get --no-install-recommends install -y udisks2
